<?php

namespace Drupal\Tests\workbench_reviewer\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\content_moderation\Functional\ModerationStateTestBase;

/**
 * Tests for the node form.
 *
 * @group workbench_reviewer
 */
class WorkbenchReviewerFormTest extends ModerationStateTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'workbench_reviewer',
    'views',
  ];

  /**
   * Test user with edit privileges.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $privilegedUser;

  /**
   * Test user without edit privileges.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $unprivilegedUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create content types for testing.
    $this->drupalLogin($this->adminUser);
    $this->createContentType(['type' => 'moderated']);
    $this->createContentType(['type' => 'unmoderated']);
    $this->createContentTypeFromUi('Moderated content', 'moderated_content', TRUE);
    $this->grantUserPermissionToCreateContentOfType($this->adminUser, 'moderated_content');
    $this->createContentTypeFromUi('Unmoderated content', 'unmoderated_content', FALSE);
    $this->grantUserPermissionToCreateContentOfType($this->adminUser, 'unmoderated_content');

    // Create additional users for testing.
    $permissions = [
      'view latest version',
      'view any unpublished content',
      'access content overview',
      'view all revisions',
      'access administration pages',
      'use editorial transition create_new_draft',
      'use editorial transition publish',
      'use editorial transition archive',
      'use editorial transition archived_draft',
      'use editorial transition archived_published',
      'create moderated_content content',
      'edit any moderated_content content',
      'create unmoderated_content content',
      'edit any unmoderated_content content',
    ];
    $this->privilegedUser = $this->drupalCreateUser($permissions);

    $permissions = [
      'create unmoderated_content content',
      'edit any unmoderated_content content',
    ];
    $this->unprivilegedUser = $this->drupalCreateUser($permissions);

    // Since we installed before configuration, flush cache.
    drupal_flush_all_caches();
  }

  /**
   * Test that the form field exists as expected.
   */
  public function testFormField() {
    $this->drupalLogin($this->adminUser);

    // Moderated content should not allow assignment.
    $this->drupalGet('/node/add/unmoderated_content');
    $this->assertSession()->fieldNotExists('moderation_state[0][state]');
    $this->assertSession()->fieldNotExists('workbench_reviewer[0][target_id]');

    // Moderated content should allow assignment.
    $this->drupalGet('/node/add/moderated_content');
    $this->assertSession()->fieldExists('moderation_state[0][state]');
    $this->assertSession()->fieldExists('workbench_reviewer[0][target_id]');

    // Create a node and assign to the privileged user.
    $edit = [
      'title[0][value]' => 'test moderated content',
      'moderation_state[0][state]' => 'draft',
      'workbench_reviewer[0][target_id]' => $this->privilegedUser->label() . ' (' . $this->privilegedUser->id() . ')',
    ];
    $this->submitForm($edit, 'Save');

    // This node should now appear in the user's list.
    $this->drupalLogin($this->privilegedUser);
    $this->drupalGet('/admin/content/assigned-to-me');
    $this->assertSession()->pageTextContains('test moderated content');

    // The unprivileged user cannot see the list.
    $this->drupalLogin($this->unprivilegedUser);
    $this->drupalGet('/admin/content/assigned-to-me');
    $this->assertSession()->statusCodeEquals(403);
  }

}
